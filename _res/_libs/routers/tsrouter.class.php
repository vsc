<?php
/**
 * @package ts_routers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
usingPackage ('controllers');

class tsRouter {
	protected	$routes = array(),
				$defaultRoute;

	public function __construct () {
	}

	public function addRoute ($ctrlName, $incRoute = null, $default = null) {
		if (empty ($ctrlName)) {
			return false;
		}

		if (empty ($incRoute))
			$incRoute = '/'.$ctrlName.'/';

		if ($default)
			$this->defaultRoute = $ctrlName;

		$this->routes[$ctrlName] = $incRoute;

		return true;
	}

	public function getController () {
		$to = urldecode (tsController::getRequest(NAV_VAR));
//		var_dump(tsController::getRequest(NAV_VAR));die;

		// set the custom routes
		tsUrlFactory::$routes = $this->routes;
		if ($this->validController ($to)) {
			return tsController::getInstance ($to);
		}

		foreach ($this->routes as $ctrlName => $regex) {
			$controllerMatch	= preg_match('/'.$ctrlName.'/i', $to);
			$regexMatch			= preg_match($regex, $to, $matches);
//			if ($matches)
//				var_dump($regex, $matches);
			if (( $controllerMatch || $regexMatch) && $this->validController ($ctrlName)) {
				array_shift($matches);
				return tsController::getInstance ($ctrlName, $matches);
			}
		}

//		die;
		if ($this->validController ($this->defaultRoute)) {
			return new $this->defaultRoute;
		}
		if ($this->validController ('index')) {
			// try index if it exists
			return tsController::getInstance ('index');
		}
	}

	public function validController ($ctrlName) {
		$ctrlName = strtolower($ctrlName);

		if (!empty ($ctrlName)) {
			return usingClass ($ctrlName);
		}

		return false;
	}
}
