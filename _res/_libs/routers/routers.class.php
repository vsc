<?php
/**
 * @package ts_routers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class routers extends tsPackage {
	protected $members = array (
		'tsRouter',
	);
}