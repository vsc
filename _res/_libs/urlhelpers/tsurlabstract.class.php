<?php
/**
 * @desc An abstract class for generating urls - it will be inherited by
 * 			friendlyUrl and regularUrl
 *
 * @author Marius Orcsik <marius@habarnam.ro>
 * @package libvsc
 * @license GPL
 */


abstract class tsUrlAbstract {
	protected	$glue,
				$separator,
				$script,
				$dir,
				$host,
				$scheme = 'http',
				$parameters = '',
				$arrParameters;

	public function __construct () {
//		var_dump($_SERVER);
		$this->script	= basename ($_SERVER['SCRIPT_NAME']);
		$this->dir		= dirname ($_SERVER['SCRIPT_NAME']);
		if ($this->dir == '/') {
			// if we are in the top folder remove the extra /
			$this->dir = '';
		}

		$this->port		= (int)$_SERVER['SERVER_PORT'];
		if ($this->port == 80 || $this->port == 443) {
			// set scheme to https if we are on secure http
			$this->setHttps(($this->port == 443));
			// if we are on default port remove it from URL
			$this->port = null;
		} else {
			$this->port = ':' . $this->port;
		}

		$this->host		= $_SERVER['HTTP_HOST'];

		$this->arrParameters = $this->getParameters();
	}

	public function __destruct () {}

	public function __toString () {
		return $this->setRequest($this->arrParameters);
	}

	public function setHttps ($true = true) {
		$true ? $this->scheme = 'https' : 'http';
	}

	abstract protected function outputParams ($params = array());

	abstract protected function getParameters ();

	public function setRequest ($params = array ()) {
		if (sizeof ($params) == 0)
			return false;

		$label = '';

		if (!empty($params[0]) && strpos($params[0], '../') === 0) {
			// corner case for urls in parent folder
			if (strlen($params[0]) > 3) {
				// we actually have a folder
				$tdir = substr ($params[0],2);
			} else {
				$tdir = '';
			}
			unset($params[0]);
		} else {
			$tdir = $this->dir;
		}
		$keys = array_keys($params);
		$lastKey = end($keys);
		return $this->scheme . '://' . $this->host . $this->port . $tdir . $this->outputParams ($params) . '';
	}

	public function getRequest ($val = null) {
//		var_dump($this->arrParameters);
		if (!empty($this->arrParameters[$val]))
			return $this->arrParameters[$val];
		else
			return false;
	}
}