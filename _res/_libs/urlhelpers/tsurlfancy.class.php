<?php
/**
 * @desc Object to handle fancy friendly url requests
 * @author Marius Orcsik <marius@habarnam.ro>
 * @package ts_urlhelpers
 * @date 09.02.27
 */
class tsUrlFancy extends tsUrlFriendly {
	private $routes = array ();

	public function addRoutes ($regexRoutes = array()) {
		if (!is_array($regexRoutes) || sizeof ($regexRoutes) == 0)
			return false;
		$this->routes = $regexRoutes;
	}

	/**
	 * uses the regex for $controller controller to replace the subpatterns
	 * with array members
	 *
	 * the output is passed to the parent::setRequest method
	 * @see _res/_libs/urlhelpers/tsUrlFriendly#setRequest()
	 */
	public function setRequest ($array = array()) {
		$controller	= array_shift($array);
		$action 	= array_shift($array);

		var_dump($this);
		return parent::setRequest ($array);
	}
}