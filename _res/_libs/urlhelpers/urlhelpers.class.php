<?php
/**
 * @package ts_urlhelpers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class urlHelpers extends tsPackage {
	protected $members = array (
		'tsUrlAbstract',
		'tsUrlFactory',
		'tsUrlFriendly',
		'tsUrlRegular'
	);
}