<?php
/**
 * @desc Object to handle friendly url requests
 * @author Marius Orcsik <marius@habarnam.ro>
 * @package ts_urlhelpers
 * @date 09.02.27
 */


class tsUrlFriendly extends tsUrlAbstract {
	protected 	$glue		= ':',
				$separator	= '/';

	public function __construct () {
		parent::__construct ();
	}

	protected function outputParams ($params = array()) {
		$this->parameters = '';
		// we have a #id as the last param
//		$label = $this->hasLabel ($params);

		foreach ($params as $param => $value) {
			if (!empty ($value)) {
				$this->parameters .= $this->separator . (!is_numeric($param) ? $param . $this->glue : '') . $value;
			}
		}

		return $this->parameters;
	}

	protected function getParameters () {
		$rArr = false;
		// removing the dir and script name + first / of the parameters
//		$stringParams = substr($_SERVER['REDIRECT_URI'], strlen($_SERVER['SCRIPT_NAME']+1));
		$params = explode ($this->separator, $_SERVER['QUERY_STRING']);

		foreach ($params as $k => $param) {
			if (!empty($param)) {
				if (strpos ($param, $this->glue)) {
					list($key,$val) = explode ($this->glue, $param);
					$rArr[$key] = $val;
				} else {
					$rArr[$k] = $param;
				}
			}
		}

		return $rArr;
	}

	public function getRequest ($val = null) {
		// for url friendly the navigation variable is the first parameter
		if ($val === NAV_VAR) {
			$val = 0;
		}
		// for url friendly the action var is the second parameter
		if ($val === ACT_VAR) {
			$val = 1;
		}

		if (key_exists($val,$_REQUEST))
			return $_REQUEST[$val];

		return parent::getRequest ($val);
	}

	/**
	 * method to replace the escaped regex special chars with the cars
	 * and strip the other special chars
	 * @param string $str
	 * @return string
	 */
	private function stripRegex ($str) {
		var_dump(str_replace ());
	}

	public function setRequest ($array = array()) {
		if (key_exists(NAV_VAR, $array)) {
			$array[0] = $array[NAV_VAR];
			unset ($array[NAV_VAR]);
		}
		if (key_exists(ACT_VAR,$array)) {
			$array[1] = $array[ACT_VAR];
			unset ($array[ACT_VAR]);
		}
		// TODO: here I should test if in the tsUrlFactory::$routes I have a
		// customized route for the controller ($array[0])
		// addendum: however this doesn't work because we might have more routes per controller
		// (not true, as we are using the controller name as an array key - ie, there can be only one :(
		// also there is not enough information in the route to know what to put in the subpattern placeholder.
		if (@array_key_exists ($array[0], tsUrlFactory::$routes)) {
			$regex = preg_replace ('/\(.*\)/u', '%s', tsUrlFactory::$routes[$array[0]]);

			$test = sprintf($regex, $array[0]);
		}
		return parent::setRequest ($array) . (!stristr(end($array), '#') ? '/' : '');
	}
}
