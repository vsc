<?php
/**
 * @desc Factory object to return either handler for friendly urls or regular
 *
 * @author Marius Orcsik <marius@habarnam.ro>
 * @package libvsc
 * @license GPL
 */
define ('URL_REWRITE', 1);
define ('URL_REGULAR', 2);

define ('URL_METHOD', URL_REWRITE);

class tsUrlFactory {
	static private	$instance	= null;
	static public	$routes		= array();

	public function __construct () {}
	public function __destruct () {}

	/**
	 * @return tsUrlAbstract
	 */
	static public function getUrlHandler () {
		if (!tsUrlFactory::$instance) {
			if (tsUrlFactory::hasRewrite ()) {
				tsUrlFactory::$instance = new tsUrlFriendly ();
			} else {
				tsUrlFactory::$instance = new tsUrlRegular ();
			}
		}
		return tsUrlFactory::$instance;
	}

	static private function hasRewrite () {
//		if (tsUrlFactory::isServer ('lighttpd')) {
//			// we have lighttpd
//			return key_exists('REDIRECT_URI' , $_SERVER);
//		} elseif (tsUrlFactory::isServer ('apache')) {
//			// we have apache
//			// this behaviour broken - because apache doesn't add this
//			// variable for urls which haven't been rewrote (i.e. the / path on
//			// the site).
//			return key_exists('REDIRECT_SCRIPT_URI' , $_SERVER);
//		}

		if (URL_METHOD == URL_REWRITE)
			return true;
		return false;
	}

	static private function isServer ($serverName) {
		return stristr($_SERVER['SERVER_SOFTWARE'], $serverName);
	}
}