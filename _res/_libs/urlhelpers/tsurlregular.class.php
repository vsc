<?php
/**
 * @desc The object to handle regular url requests
 *
 * @author Marius Orcsik <marius@habarnam.ro>
 * @package ts_urlhelpers
 * @date 09.02.27
 */

class tsUrlRegular extends tsUrlAbstract {
	protected	$glue		= '=',
				$separator	= '&',
				$parameters = '/?';


	public function __construct () {
		parent::__construct ();
	}

	protected function outputParams ($params = array()) {
		// getting the first element
		$beginParams = current ($params);
		foreach ($params as $param => $value) {
			if ($value != $beginParams) {
				// if we are not at the first parameter attach the separator
				$this->parameters .= $this->separator;
			}

			if ($param === 0) {
				$param = NAV_VAR;
			}

			if ($param === 1) {
				$param = ACT_VAR;
			}

			$this->parameters .=  (is_string ($param) ? $param . $this->glue : ''). $value;
		}

		return $this->parameters;
	}

	protected function getParameters () {
		return $_GET;
	}
}