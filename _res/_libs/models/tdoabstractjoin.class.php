<?php
/**
 * @package ts_models
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class tdoAbstractJoin {
	static public $validTypes = array (
		'INNER',
		'LEFT',
		'RIGHT',
		);

	/**
	 * @var tdoAbstract $leftTable
	 * @var tdoAbstract $rightTable
	 * @var tdoAbstractField $leftField
	 * @var tdoAbstractField $rightField
	 */
	protected	$state,
				$type,

				$leftTable,
				$rightTable,

				$leftField,
				$rightField;

	public function __construct ($type, &$lt, &$rt, &$lf, &$rf, $state) {
		if (
			$rt instanceof tdoAbstract ||
			$lt instanceof tdoAbstract
		) {
			$this->leftTable	= &$lt;
			$this->rightTable	= &$rt;
		}
		if (tdoAbstractJoin::isValidType($type))
			$this->type = $type;

		if (
			$lf instanceof tdoAbstractField &&
			$rf instanceof tdoAbstractField
		) {
			$this->rightField	= &$rf;
			$this->leftField	= &$lf;
		}

		$this->state	= $state;

		$this->composeFields ();
		$this->composeWheres ();

	}

	public function __destruct () {}

	public function __toString() {
		$lAlias = $this->leftTable->get_alias();

		return (string)$this->type.' JOIN '.$this->rightTable->get_name().
			' AS t'.$this->state.' ON '.(isset($lAlias) ? $lAlias : $this->leftTable->get_name()).
			'.'.$this->leftField->name.
			' = t'.$this->state.'.'.$this->rightField->name.' ';
	}

	/**
	 * Will compose the $rightTable and $leftTable fields
	 * @return void
	 */
	public function composeFields () {
		$leftFields		= $this->leftTable->get_members();

		$this->rightTable->set_alias($this->state);
		$rightFields	= $this->rightTable->get_members();

		$this->leftTable->addFields($rightFields);
	}

	/**
	 * Will compose the $rightTable and $leftTable WHERE clauses
	 * @return void
	 */
	public function composeWheres () {
		if (!is_array($this->leftTable->wheres))
			$this->leftTable->wheres =  array();
		if (!is_array($this->rightTable->wheres))
			$this->rightTable->wheres =  array();

		//		var_dump($this->rightTable->wheres, $this->leftTable );die;
		foreach ($this->rightTable->wheres as $where) {
			$this->leftTable->addWhere ($where);
		}
		$this->leftTable->wheres = array_merge($this->rightTable->wheres, $this->leftTable->wheres);
	}

	public function set_state ($st) {
		$this->state	= $st;
	}

	static public function isValidType ($inc) {
		if (in_array($inc, tdoAbstractJoin::$validTypes))
		return true;
		return false;
	}
}
