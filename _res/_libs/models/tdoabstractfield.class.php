<?php
/**
 * @package ts_models
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
define ('INDEX',	1);
define ('PRIMARY',	2);
define ('UNIQUE',	4);
define ('FULLTEXT',	8);

class tdoAbstractField {
	static public $validTypes = array (
		'VARCHAR',
		'INT',
		'DATE',
		'TEXT',
		'FLOAT',
		'TIMESTAMP',
		'ENUM'
		);

	protected	$name, $type, $flags, $value, $table, $modifier = null, $order = null, $group = null, $where = false;

	public function __construct ($incName, $incTable, $incType='INT', $incFlags=0) {
		$this->name		= $incName;
		$this->table	= $incTable;
		$this->type		= $incType;
		$this->flags	= $incFlags;
	}

	public function __set ( $key, $value ) {
//		var_dump($key, $value);
		if ( array_key_exists ($key, get_object_vars($this)) ) {
			$this->$key = $value;

//						if ($key == 'where')
//							var_dump($this);

			if ( is_null ($this->type) ) {
				$this->setType();
			}

			return true;
		} else
		return false;
	}

	public function __get ( $key ) {
//		if ($key == 'value' && !$this->value)
//			return null;
		return $this->$key;
	}

	public function __call ( $method, $args) {
		$all = get_object_vars($this);

		if ( preg_match( '/set_(.*)/', $method, $found ) ) {
			if  (array_key_exists( $found[1], $all)){
				$this->$found[1] = $args[0];
				return true;
			}
		} elseif ( preg_match( '/get_(.*)/', $method, $found ) ) {
			if  (array_key_exists( $found[1], $all)){
				return $this->$found[1];
			}
		}
		return false;
	}

	public function __destruct () {}

	public function __toString () {
		return (string)$this->value;
	}

	//	public function inWhere () {
	//		return $this->where;
	//	}

	static public function isValidType ($inc) {
		if (in_array($inc, tdoAbstractField::$validTypes)){
			return true;
		}
		return false;
	}

	public function set_modifier ($modif) {
		//		if (stristr($modif, '%'))
		$this->modifier = $modif;
	}

	public function set_value ($value) {
		$this->value = $value;
	}

	public function set_group ($true = true) {
		$this->group = (bool)$true;
	}

	public function set_order ($asc = true) {
		$this->order = (bool)$asc;
	}

	public function isIndex() {
		return (($this->flags & INDEX) == INDEX);
	}

	public function isPrimary() {
		return (($this->flags & PRIMARY) == PRIMARY);
	}

	public function isFullText() {
		return (($this->flags & FULLTEXT) == FULLTEXT);
	}

	public function isUnique () {
		return (($this->flags & UNIQUE) == UNIQUE);
	}

	public function setType () {
		$incValue = $this->value;
		// TODO : enums
		if (is_int($incValue)) {
			$this->type = 'INT';
		} elseif (is_float($incValue)) {
			$this->type = 'FLOAT';
		} elseif (is_string($incValue)) {
			if (strtotime($this->value)){
				$this->type = 'DATE';
			}elseif (strlen($incValue) > 255)
			$this->type = 'TEXT';
			else
			$this->type = 'VARCHAR';
		}
	}
}
