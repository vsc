<?php
/**
 * @desc The Abstract Data Objects series
 * @package ts_models
 * @author Marius Orcsik <marius@habarnam.ro>
 * @version 0.0.1
 */

class tdoAbstract {
	/**
	 * @var interfaceSql
	 */
	public $db;
	public $wheres = array();
	public $groups;
	public $limit;
	public $refers = array();

	/**
	 * @var tdoAbstractField
	 */
	protected $id;
	protected $name;
	protected $alias;
	protected $fields;

	/**
	 * Function to implement get_ | set_ virtual methods
	 * @param string $method
	 * @param [] $args
	 * @return mixed
	 * @see  http://www.ibm.com/developerworks/xml/library/os-php-flexobj/ by Jack Herrington <jherr@pobox.com>
	 */
	function __call ($method, $args) {
		$diff = $this->get_members();
		$all = get_object_vars($this);

		if ( preg_match( '/set_(.*)/', $method, $found ) ) {
			// check for fields with $found[1] name
			if ( array_key_exists( $found[1], $diff) ) {
				$this->fields[$found[1]]->setValue($args[0]);
				return true;
				// check for obj members with $found[1] name
			} elseif  (array_key_exists( $found[1], $all)){
				$this->$found[1] = $args[0];
				return true;
			}
		} else if ( preg_match( '/get_(.*)/', $method, $found ) ) {
			if ( array_key_exists( $found[1], $diff ) ) {
				return $this->fields[$found[1]]->getValue();
			} elseif  (array_key_exists( $found[1], $all)){
				return $this->$found[1];
			}
		}
		return false;
	}

	/**
	 * @desc A function to implement a virtual getter member of the class
	 *
	 * @param string $key
	 * @return mixed
	 *
	 * @see  http://www.ibm.com/developerworks/xml/library/os-php-flexobj/ by Jack Herrington <jherr@pobox.com>
	 */
	function __get ( $key ) {
		// fixed a bug where the method wasn't working for $key == 'id' (which doesn't exist in fields array)
		if ($key == 'id')
			return $this->id;
		return $this->fields[$key];
	}

	/**
	 * @desc A function to implement a virtual setter member for this class
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return bool
	 *
	 * @see  http://www.ibm.com/developerworks/xml/library/os-php-flexobj/ by Jack Herrington <jherr@pobox.com>
	 */
	function __set ( $key, $value ) {
		if (
			array_key_exists ($key, $this->get_members()) /*&&
			$this->isValidMember($this->fields[$key])*/
		) {
			$this->fields[$key]->set_value ($value);
			return true;
		} else {
			$this->fields[$key] = new tdoAbstractField ($key, $this->alias);
			$this->fields[$key]->set_value ($value);
		}
	}

	public function __construct(&$db) {
		$this->db		= &$db;
		$this->alias	= 'm1';

		$this->instantiateMembers();
	}

	public function __destruct() {}

	/**
	 * gets the members we consider table fields
	 *
	 * @return array ('fieldName' => tdoAbstractField)
	 */
	public function &get_members () {
		return $this->fields;
	}

	/**
	 * checks if a field is a valid member of the current object
	 *
	 * @param tdoAbstractField $incMember
	 * @return bool
	 */
	public function isValidMember ($incMember) {
		if (
			! ($incMember instanceof tdoAbstractField ) ||
			// this prevents an error in php > 5.2 object comparison
			! in_array ($incMember, $this->get_members() /**/, true/**/)
		) {
			return false;
		} else
			return true;
	}

	/**
	 * instantiating the object's members
	 */
	public function instantiateMembers () {
		foreach ($this->get_members() as $key => $field) {
			if ( is_int ($key) && is_string ($field) ) {
				$this->fields[$field] = new tdoAbstractField ($field, $this->alias);

				// FIXME: this is so bad :D - it implies that the primary key must be the first field containing _id :D
				if (stristr ($field,'_id') && !isset ($this->id) && $this->isValidMember ($this->fields[$field])) {
					$this->id = &$this->fields[$field];
				}
				unset ($this->fields[$key]);
			}
		}

		$this->wheres		= array();
		$this->groups		= '';
//		$this->orders		= '';
		$this->refers		= array();
	}
	/**
	 * setting the table's index field
	 *
	 * @param tdoAbstractField $obj
	 */
	public function set_index (&$obj) {
		if ($this->isValidMember($obj)) {
			$this->id 	= &$obj;
			$this->id->flags = PRIMARY;
		}
	}

	/**
	 * setting an alias for the table in case of joins.
	 * this method also sets the alias on all the fields of the current object
	 *
	 * @param string $alias
	 */
	public function set_alias ($alias) {
		foreach ($this->get_members() as $field){
			if ($field->table == $this->alias) {
				$field->table = 't'.$alias;
			}
		}
		$this->alias = 't'.$alias;
	}

	/**
	 * For adding custom sql functions for certain fields
	 *
	 * @param string $modif
	 * @param tdoAbstractField $field
	 */
	public function set_fieldModifier ($modif, &$field) {
		if ($this->isValidMember ($field) && stristr ($modif, '%'))
		$field->set_modifier ($modif);
	}

	/**
	 * method used in join cases to add the joined object's fields
	 * to the current one
	 *
	 * @param array ('fieldName' => tdoAbstractField) $incArr
	 */
	public function addFields (&$incArr) {
		if (is_array($incArr)) {
			foreach ($incArr as $fieldName => $field) {
				$this->fields[$fieldName]	= $field;
				if (key_exists($fieldName, $this->fields)) {
					// if we have the field already in the fields array
					// we only need to keep it's alias
					$curentAlias = $this->fields[$fieldName]->get_table ();
					$this->fields[$fieldName]->set_table ($curentAlias);
				}
			}
		}
	}

	/**
	 * method to return an array of fieldNames=>fieldValues
	 * @return array
	 */
	public function fieldsAsArray () {
		foreach ($this->fields as $key => $val) {
			$ret[$key] = $val->value;
		}
		return $ret;
	}

	/**
	 * Based on field values, we get the _first_ row in the table that matches
	 * TODO: maybe we can have a function that returns _all_ the rows that match
	 *
	 * @return bool
	 */
	public function buildObj () {
		$sql = $this->buildSql(1);
		$this->db->query( $sql );

		$arr = $this->db->getAssoc();
		if (is_array($arr)) {
			foreach ($arr as $field => $var) {
				$this->fields[$field]->set_value ($var);
			}
			return true;
		}
		return false;
	}

	/**
	 * Gets the row in the table for $id
	 *
	 * @param mixed $id
	 */

	public function get ($id) {
		if (empty ($id))
			return false;
		$id = $this->db->escape($id);

		$this->instantiateMembers();

		$this->id->set_value ($id);
		$this->buildObj ();
	}
	/**
	 * Returns the last id of the table
	 * OBS: this assumes that we didn't delete any rows from the table.
	 *
	 * @return int
	 */
	public function getLastInsertedId () {
		$sql = $this->db->_SELECT ($this->id->name)
			.$this->db->_FROM ($this->name) . $this->db->_AS ($this->alias);

		$this->addOrder ($this->id, false);

		$sql .= $this->db->_ORDER ($this->outputOrders ());

		$sql .= $this->db->_LIMIT (1);

		$this->db->query ($sql);
		return $this->db->getScalar ();
	}

	/**
	 * encapsulating the $this->db->escape() method
	 *
	 * @param mixed $value
	 * @return mixed
	 */
	public function escape ($value) {
		if (is_numeric ($value)) {
			return (int)$value;
		} else {
			// this behavior is broken for PostgreSQL as it encloses the field values' in apostrophes
			// TODO for each driver either:
			// 1. develop a static method for escaping variables
			// 2. either treating all cases in this method (probably 1)
			return $this->db->STRING_OPEN_QUOTE . $this->db->escape ($value) . $this->db->STRING_CLOSE_QUOTE;
		}
	}
	/**
	 * inserting into the database
	 * TODO: multiple inserts to use with loadFromArray
	 *
	 * @return int
	 */
	public function insert () {
		$sql = $this->db->_INSERT ($this->name);

		$fieldStr = '';
		$valueStr = '';
		$values = array ();
		$f = $this->get_members();

//		$sql .= ' '.$this->outputRefers().' SET';

		foreach ($f as $fieldName => $field) {
			if ($this->isValidMember ($field) && ($field != $this->id) && !is_null ($field->value)) {
//				$valueStr .= $this->escape ($field->value);
				$value = $field->value;

				$fieldStr.= (!empty ($fieldStr) ? ', ' : ' ') . $fieldName;
				$valueStr.= (!empty ($valueStr) ? ', ' : ' ') . $value;
				$values[] = $value;
			}
		}

//		$sql.= ' ('.$fieldStr.') VALUES ('.$value.')';
		$sql.= ' ('.$fieldStr.')' . $this->db->_VALUES ($values); // VALUES ('.$valueStr.')';
		if ($fieldStr) {
			$this->db->query($sql);
			return $this->getLastInsertedId();
		}
	}

	public function update ($id = null) {
		if (is_null ($id)) {
			$id = $this->id->value;
		}

		if (is_null ($id)) {
			throw new Exception('Cannot update record in table '.$this->name.' because an id hasn\'t been provided');
			return false;
		}

		$sql = $this->db->_UPDATE (array ($this->name, $this->alias) );

		if (is_array ($this->refers) && !empty ($this->refers)){
			$this->refers = array_reverse ($this->refers);

			foreach ($this->refers as $ref)
			$sql .= $ref;
		}

		$sql .= $this->db->_SET();

		$fields = $this->get_members();

		foreach ($fields as $fieldName => $field) {
			if (($field instanceof tdoAbstractField) && $field != $this->id) { // TODO: make a more real check for field is an id
				$value = $field->value;
			}

			if ((isset($value) && !is_null($value))) {
				$sql.= $field->table.'.'.$fieldName.' = '.$this->escape ($value).', ';
			}
		}
//		echo substr ($sql,-2).'<br/><br/>';
		if (substr ($sql,-2) == ', ') {
			$sql = substr ($sql, 0, -2);
		}
//		die;
		$sql.= $this->db->_WHERE($this->alias.'.'.$this->id->name.' = '.$this->escape($id));

//		var_dump($sql);die ('<br/>update');
		$this->db->query($sql);

		return $id;
	}

	public function replace ( $id = null ) {
		if (is_null($id)) {
			$id = $this->id->value;
		}

		if (is_null($id) || !$this->idExists ($id) ) {
			return $this->insert ();
		} else {
			return $this->update ($id);
		}
	}

	// TODO: make this the same way the find first method works
	public function delete ($id = null) {
		$id = (!is_null ($id) ? $id :$this->id->value);

		if (!is_null ($id)) {
			// no need for other wheres
			$this->wheres = array();
			$this->addWhere ($this->id, '=', $id);
		}

		if (empty($this->wheres)) {
			return false;
		}

		$temp = $this->alias;
		$this->set_alias(null);


		$sql = 'DELETE FROM '.$this->name.' WHERE '; //$this->id->name.' = '.$this->escape($value).' LIMIT 1';
		$sql.= $this->outputWheres();
//		echo $sql;die;
		$affRows = $this->db->query($sql);

		$this->set_alias($temp);
		return $affRows;
	}

	public function reset () {
		foreach ($this->fields as $key => $field) {
			if ($field instanceof tdoAbstractField) {
				$this->fields[$key] = new tdoAbstractField($key, $this->alias);
			}
		}

		$this->wheres		= array();
		$this->groups		= array();
//		$this->orders		= array();
		$this->refers		= array();
		return true;
	}

	public function idExists ($id = null) {
		if (is_null($id)) {
			$id = $this->id->value;
		}
		if (is_null($id))
			return false;

		$this->id->set_modifier ('COUNT(%s)');

		$t = sprintf ($this->id->modifier,  $this->id->name);

		$sql	= $this->db->_SELECT ($t).
				$this->db->_FROM ($this->name).
				$this->db->_WHERE ($this->id->name.' = '.$this->escape($id));

		$this->db->query($sql);

		if ($this->db->getScalar()) {
			return true;
		} else {
			return false;
		}
	}

	protected function outputFieldList () {
		$fields = '';
		$f		= $this->get_members();
		//		var_dump($f);
		foreach ($f as $fieldName => $field) {
			if ($this->isValidMember ($field)) {

				if (!is_null ($field->modifier)) {
					// i replaced str_replace ('%s', $curField, '%s', $curField)
					// as sometimes I might need it for something else than %s
					$fields .= sprintf ($field->modifier, (!is_null ($field->table) ? $field->table . '.' : '') . $field->name) .
					$this->db->_AS($field->name) . ', ';
				} elseif ( !$field->inWhere ()) {
					$fields .= (!is_null ($field->table) ? $field->table.'.' : '') . $field->name . ', ';
				}

			} else {
				trigger_error ($fieldName . ' is not a valid member of ' . get_class($this));
//				throw new Exception ($fieldName . ' is not a valid member of ' . get_class($this));
				return false;
			}
		}
		return substr ($fields,0,-2);//$fields;
	}

	protected function outputWheres ($bIW = true) {
//		var_dump($this->wheres);
		return implode ($this->db->_AND(), $this->wheres);
	}

	protected function outputGroups () {
		// groups
		$groups = '';
		$f = $this->get_members ();
		foreach ($f as $field) {
			if (!is_null ($field->group) ) {
				$groups .= (!empty($groups) ? ', ' : '') .
						$field->table . '.' . $field->name;
			}
		}

		return $groups;
	}

	protected function outputOrders () {
		$orders = '';
		$f = $this->get_members();
		foreach ($f as $field)
		if (!is_null($field->order)) {
			$orders .= (!empty($orders) ? ', ': '') .
					$field->table . '.' . $field->name .
					($field->order == true ? ' ASC' : ' DESC');
		}
		return $orders;
	}

	protected function outputRefers () {
		$refers= '';
		if (!empty($this->refers) && is_array($this->refers)){
			$rs = array_reverse ($this->refers);
			foreach ($rs as $ref) // using the __toString magic function
				$refers .= $ref;
		}
		return $refers;
	}

	protected function buildInherentWheres () {
		$diff	= $this->get_members();
		// let's hope this doesn't break stuff.
		// it's needed when we use more queries on the same instance of the object :D

		if (is_array ($diff)) {
			foreach ($diff as $fieldName => $field) {
				if ( $this->isValidMember ($field) ) {
					if (!is_null($field->value)) {
						$this->addWhere ($field, '=', $this->escape ($field->value));
					}
				} else {
					trigger_error ($fieldName . ' is not a valid member of ' . get_class($this));
					throw new Exception ($fieldName . ' is not a valid member of ' . get_class($this));
				}
			}
		}

		// fix a bug where if we were calling a object->get(null)
		// it ended by having a query with "where 1" - wich resulted in gettina all rows - BAD
		if (empty ($this->wheres)) {
			$t = $this->db->FALSE;
			$this->wheres[] = new tdoAbstractClause ($t);
		}
	}

	/**
	 * function to add an abstract clause to the current object if it doesn't exist
	 *
	 * @param tdoAbstractField|tdoAbstractClause $field1
	 * @param string $condition
	 * @param string $field2
	 */
	public function addWhere (&$field1, $condition = null, $field2 = null) {
		if (($field1 instanceof tdoAbstractClause) && ($condition == null || $field2 == null)) {
			$w = &$field1;
		} else {
			$w = new tdoAbstractClause ($field1, $condition, $field2);
		}
		// this might generate an infinite recursion error on some PHP > 5.2 due to object comparison
		if (!in_array ($w, $this->wheres /*, true */)) {
			$this->wheres[]	= &$w;
		}
	}

	public function addOrder (&$orderField, $asc = true) {
		if (!($orderField instanceof tdoAbstractField) && is_string ($orderField)) {
			$orderField = &$this->fields[$orderField];
		}
		if ($this->isValidMember ($orderField))
			$orderField->set_order ($asc);
	}

	public function addGroup (&$groupField) {
		if (!($groupField instanceof tdoAbstractField) && is_string ($groupField)) {
			$groupField = &$this->fields[$groupField];
		}
		if (($groupField instanceof tdoAbstractField)) {
			$groupField->set_group (true);
		}
	}

	/**
	 * @param int $start
	 * @param int $count
	 */

	public function addLimit ($start = 0, $count=null) {
		if (empty($this->limit))
			$this->limit = $this->db->_LIMIT ($start, $count);
	}

	/**
	 * building a normal SELECT query
	 *
	 * @param int $start
	 * @param int $end
	 * @return string
	 */
	protected function buildSql ($start = 0, $count = 0) {
		$sql = $this->db->_SELECT($this->outputFieldList()). ' FROM '.$this->name.' AS '.$this->alias.' ';

		$this->buildInherentWheres(); // will it work

		$sql .= $this->outputRefers();

		$sql .= $this->db->_WHERE ($this->outputWheres());

		$sql .= $this->db->_GROUP ($this->outputGroups());

		$sql .= $this->db->_ORDER ($this->outputOrders());

		if (empty ($this->limit)) {
			$this->addLimit ($start, $count);
		}
		$sql .= $this->limit;
		return $sql;
	}

	public function find ($start = 0, $count = 0) {
		$result = $this->db->query ($this->buildSql(), $start, $count);
		return $result;
	}

	public function findFirst () {
		$this->buildInherentWheres();

		$this->db->query($this->buildSql(), 0, 1);
		$row = $this->db->getAssoc();
		if (is_array($row))
			foreach ($row as $field => $value){
				$this->fields[$field]->value = $value;
			}
	}

	public function getArray ($start = 0, $count = 0, $orderBy = null) {
//		$this->buildInherentWheres ();
		// generally when we getArray and don't have any where clauses
		// we would like _all_ rows
//var_dump((string)$this->wheres[0], sizeof ($this->wheres) );
		if (empty ($this->wheres) || (sizeof ($this->wheres) == 1 && (string)$this->wheres[0] == '0'))
			$this->wheres[] = new tdoAbstractClause ($this->db->TRUE);
		$sql = $this->buildSql ($start, $count, $orderBy);




		$this->db->query ($sql);
		$ret = $this->db->getArray ();

		// in the case of an empty return - we make sure that we
		// have the keys we need in what we return
		if (!is_array($ret)) {
			$ret[0] = $this->fieldsAsArray ();
		}

		return $ret;
	}

	/**
	 * execute a select count () on the current object
	 *
	 * @return null
	 */

	public function getCount() {
		// this is bad:
		// it takes into account the counting rows in many2many table relationshit
		// but it does not for more than one group by
		foreach ($this->get_members() as $fieldName => $field) {
			if ($field->get_group() == true) {
				$what = 'DISTINCT(' . $field->table .'.' . $fieldName . ')';
			}
		}

		if (empty ($what))
			$what = '*';

		// made it a bit more clean and less sql portable by adding hard coded
		// MY SQL stuff
		$sql = $this->db->_SELECT (' COUNT(' . $what . ') ');

		$sql .= $this->db->_FROM( $this->name. $this->db->_AS($this->alias) );

		$this->buildInherentWheres();

		$sql .= $this->outputRefers();

		$sql .= $this->db->_WHERE ($this->outputWheres());

		// this would need to be replaced with a count(distinct(group by column))
		// because we're using many2many relations
		// also I do not know how this behaves for:
		// 1. multiple group by's
		// 2. !many2many relations.
//		$sql .= $this->db->_GROUP ($this->outputGroups());
//		echo $sql; die;
		$this->db->query($sql);
		return $this->db->getScalar();
	}

	public function getVector() {
		// I really don't see why we need this. ?
	}

	/**
	 * Function to load the values of current object from an array
	 * of type field_name => field_value
	 * If strict is false, the current object can have fields that are not already
	 * present in $this->fields[]
	 *
	 * @param array $valArray
	 * @param bool $strict
	 */
	public function loadFromArray ($valArray, $strict = true) {
		foreach ($valArray as $fieldName => $value) {
			if (array_key_exists ($fieldName, $this->fields)) {
				$this->fields[$fieldName]->set_value($value);
			} elseif (!$strict) {
				// if the field name is not in the field list of the current object
				// it means that the valArray object is got from an JOIN sql
				$this->fields[$fieldName] = new tdoAbstractField ($value,'j1');
				$this->fields[$fieldName]->set_value ($value);
			}
		}
	}

	/**
	 * FIXME: make it work when composing with the same object
	 *
	 * @param tdoAbstract $incOb
	 * @return void
	 */
	private function composeObj ($incOb) {
		if (!($incOb instanceof tdoAbstract))
			return false;
		$refs  		= count($this->refers);

		foreach ($incOb->refers as $alias => $ref) {
			$tAl				= $refs++;

			$this->refers[$tAl]	= $ref;//str_replace(array($alias, $aliases[$alias][1]), array($tAl, $aliases[$alias][2]), $ref);
			$this->refers[$tAl]->set_state ($tAl);
		}
	}

	/**
	 * Function to execute a join between two tdoAbstract objects
	 *
	 * @param string $jType
	 * @param tdoAbstractField $thisJField
	 * @param tdoAbstract $incOb
	 * @param tdoAbstractField $incObJField
	 * @return unknown
	 */
	public function joinWith ($jType = null, &$thisJField = null, &$incOb = null, &$incObJField = null) {
		if (
			!tdoAbstractJoin::isValidType ($jType) ||
			!$this->isValidMember ($thisJField)
		) return false;

		$this->composeObj ($incOb);

		$tAl = (count($this->refers));
		if ($tAl > 59) {
			trigger_error ('Join aborted for table '.$this->name.': Too many tables; MySQL can only use 61 tables in a join', E_USER_NOTICE);
			return;
		} else {
			$incOb->set_alias($tAl);

			if ($thisJField == null || !($thisJField instanceof tdoAbstractField))
				$thisJField = $this->id;

			if ($incObJField == null || !($incObJField instanceof tdoAbstractField))
				$incObJField = $incOb->id;

			$this->refers[$tAl] 	= new tdoAbstractJoin ($jType, $this, $incOb, $thisJField, $incObJField, $tAl);
			$this->refers[$tAl]->set_state ($tAl);
		}
		return $this;
	}

	/**
	 * Wrapper method for joinWith ('INNER'..
	 * @param $thisJField
	 * @param $incOb
	 * @param $incObJField
	 * @return tdoAbstract (TODO)
	 */
	public function innerJoin (&$thisJField = null, $incOb = null, $incObJField = null) {
		if (is_string($incOb)) {
			$incOb = new $incOb ($this->db);
//			$incOb->setAlias (sizeof ($this->refers));
		}

		if (is_string($incObJField) && $this->isValidMember($incObJField)) {
			$incObJField = $incOb->$incObJField;
//			$incObJField->set_table ($incObJField->get_table());
		}

		$this->joinWith ('INNER', $thisJField, $incOb, $incObJField);
		return $this;
	}

	/**
	 * Wrapper method for joinWith ('LEFT'..
	 * @param $thisJField
	 * @param $incOb
	 * @param $incObJField
	 * @return tdoAbstract (TODO)
	 */
	public function leftJoin (&$thisJField = null, &$incOb = null, &$incObJField = null) {
		$this->joinWith ('LEFT', $thisJField, $incOb, $incObJField);
		return $this;
	}

	/**
	 * Wrapper for addWhere - assures chainability and will help migrate to
	 * a better structured object (ie, better differentiation between protected
	 * and private methods
	 * @param tdoAbstractField $field1
	 * @param string $condition
	 * @param $field2
	 * @return tdoAbstract
	 */
	public function where (&$field1, $condition, $field2) {
		$this->addWhere($field1, $condition, $field2);
		return $this;
	}


	/**
	 * Wrapper for addGroup (...
	 * @param tdoAbstractField $groupField
	 * @return tdoAbstract
	 */
	public function group (&$groupField) {
		$this->addGroup($groupField);
		return $this;
	}

	/**
	 * Wrapper for addLimit
	 * @param $start
	 * @param $count
	 * @return tdoAbstract
	 */
	public function limit ($start, $count = null) {
		$this->addLimit($start, $count);
		return $this;
	}

	/**
	 * Wrapper method for addOrder
	 * @param tdoAbstractField$orderField
	 * @param bool $asc
	 * @return tdoAbstract
	 */
	public function order (&$orderField, $asc = true) {
		$this->addOrder($orderField, $asc);
		return $this;
	}

	/**
	 * function to dump a <type>Sql
	 * problem with the field types. :D
	 *
	 * @param tdoAbstract $obj
	 *
	 static public function dumpSchema ($obj) {
		if ($obj instanceof tdoAbstract)
		throw new Exception('Can\'t generate sql dump');

		$sql = 'CREATE TABLE '. $obj->name . ' (';

		foreach ($obj->getFields() as $fieldName => $data) {
		$sql .= $fieldName.' '.$data[0].', ';
		}
		$sql .= ')';
		return $sql;
		}
		*/
}
