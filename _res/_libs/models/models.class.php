<?php
/**
 * @package ts_models
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class models extends tsPackage {
	protected $members = array (
			'sqlDrivers',
			'tdoAbstract'
		);
}