<?php

/**
 * class to abstract a where clause in a SQL query
 * TODO: add possibility of complex wheres: (t1 condition1 OR|AND|XOR t2.condition2)
 * TODO: abstract the condition part of a where clause - currently string based :D
 * @package ts_models
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class tdoAbstractClause {
	protected	$subject, $predicate, $predicative;

	/**
	 * initializing a WHERE|ON clause
	 *
	 * @param tdoAbstractClause|tdoAbstractField $subject
	 * @param string $predicate
	 * @param tdoAbstractClause|tdoAbstractField|null $complement
	 */
	public function __construct ($subject, $predicate = null, $predicative = null) {
		// I must be careful with the $subject == 1 because (int)object == 1
		if (($subject === 1 || is_string($subject)) && $predicate == null && $predicative == null) {
			$this->subject		= $subject;
			$this->predicate	= '';
			$this->predicative	= '';
			return;
		}

		if (($subject instanceof tdoAbstractField ) || ($subject instanceof tdoAbstractClause )) {
			$this->subject 	 	= &$subject;

//			$subject->set_where = true;

			$this->predicative	= &$predicative;

			if ($this->validPredicative ($predicate))
			$this->predicate	= $predicate;

		} else {
			$this->subject		= '';
			$this->predicate	= '';
			$this->predicative	= '';

			return;
		}
	}
	public function __destruct () {}

	public function __toString () {
//		var_dump($this->subject, $this->predicate, $this->predicative);
//		echo '<br/>';
		if ($this->subject === '1' || is_string($this->subject)) {
//			var_dump($this->subject);
			return (string)$this->subject;
		} elseif ($this->subject instanceof tdoAbstractClause) {
			$subStr = (string)$this->subject;
		} elseif ($this->subject instanceof tdoAbstractField) {
			// without $this->subject->table != 't' we have a bug in the delete op
			$subStr =  ($this->subject->table != 't' ? $this->subject->table.'.': '') . $this->subject->name;
		} else {
			return '';
		}

		if (is_null($this->predicative)) {
			if ($this->validPredicative ($this->predicate)) {
				$preStr = 'NULL';
			} else
			$preStr = '';
		} elseif (is_numeric($this->predicative)) {
			$preStr = $this->predicative;
		} elseif (is_string($this->predicative)) {
			$preStr = $this->predicative;

			if ($this->predicate == 'LIKE') {
				$preStr = '%'.$this->predicate.'%';
			}

			$preStr = (stripos($preStr, '"') !== 0 ? '"'.$preStr.'"' : $preStr);//'"'.$preStr.'"';
		} elseif (is_array($this->predicative)) {
			$preStr =  '("'.implode('", "',$this->predicative).'")';
		} elseif ($this->predicative instanceof tdoAbstractfield) {
			$preStr = ($this->predicative->table != 't' ? $this->predicative->table.'.': '').$this->predicative->name;
		} elseif ($this->predicative instanceof tdoAbstractClause) {
			$subStr = $subStr;
			$preStr = $this->predicative;
		}

		$retStr = $subStr.' '.$this->predicate.' '.$preStr;
		if (($this->subject instanceof tdoAbstractClause) && ($this->predicative instanceof tdoAbstractClause))
			return '('.$retStr.')';

		return $retStr;
	}

	private function validPredicative ($predicate) {
		// need to find a way to abstract these
		//		$validPredicates = array (
		//			'AND',
		//			'&&',
		//			'OR',
		//			'||',
		//			'XOR',
		//			'IS',
		//			'IS NOT',
		//			'!',
		//			'IN',
		//			'LIKE',
		//			'=',
		//			'!=',
		//			'<>'
		//		);
		if ($this->predicative instanceof tdoAbstractClause) {
			// we'll have Subject AND|OR|XOR Predicative
			$validPredicates = array (
				'and',
				'&&',
				'or',
				'||',
				'xor'
				);
		} elseif (($this->predicative instanceof tdoAbstractField) || is_numeric($this->predicative)) {
			// we'll have Subject =|!= Predicative
			$validPredicates = array (
				'=',
				'!=',
				'>',
				'<',
				'>=',
				'<='
				);
		} elseif (is_array($this->predicative)) {
			$validPredicates = array (
				'in',
				'not in'
				);
		} elseif (is_string($this->predicative)) {
			$validPredicates = array (
				'=',
				'like',
			// dates
				'>',
				'<',
				'>=',
				'<='
				);
		} elseif (is_null($this->predicative)) {
			$validPredicates = array (
				'is',
				'is not'
				);
		}

		return in_array(strtolower($predicate), $validPredicates);
		//
		//		if (in_array($predicate, $validPredicates) && (($predicative instanceof tdoAbstractClause) || ($predicative instanceof tdoAbstractField)))
		//			return true;
		//		return false;
	}
}
