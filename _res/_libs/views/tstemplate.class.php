<?php
/**
 * object to replace smarty for templating.
 * desired goal is to keep it _very_ simple.
 * @package ts_views
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class tsTemplate {
	protected 	$view;
	private 	$templateVariables = array (),
				$content = '';

	/**
	 * @desc function to implement get_ | set_ virtual methods
	 *
	 * @param string $method
	 * @param array $args
	 * @return bool|mixed
	 *
	 * @see  http://www.ibm.com/developerworks/xml/library/os-php-flexobj/ by Jack Herrington <jherr@pobox.com>
	 */
//	function __call ( $method, $args) {
//		$diff = $this->get_members();
//		$all = get_object_vars($this);
//
//		if ( preg_match( '/set_(.*)/', $method, $found ) ) {
//			// check for fields with $found[1] name
//			if ( array_key_exists( $found[1], $diff) ) {
//				$this->fields[$found[1]]->setValue($args[0]);
//				return true;
//				// check for obj members with $found[1] name
//			} elseif  (array_key_exists( $found[1], $all)){
//				$this->$found[1] = $args[0];
//				return true;
//			}
//		} else if ( preg_match( '/get_(.*)/', $method, $found ) ) {
//			if ( array_key_exists( $found[1], $diff ) ) {
//				return $this->fields[$found[1]]->getValue();
//			} elseif  (array_key_exists( $found[1], $all)){
//				return $this->$found[1];
//			}
//		}
//		return false;
//	}

	/**
	 * @desc A function to implement a virtual getter member of the class
	 *
	 * @param string $key
	 * @return mixed
	 *
	 * @see  http://www.ibm.com/developerworks/xml/library/os-php-flexobj/ by Jack Herrington <jherr@pobox.com>
	 */
	function __get ( $key ) {
		// fixed a bug where the method wasn't working for $key == 'id' (which doesn't exist in fields array)
		if (!empty ($this->templateVariables[$key])) {
			$ret = $this->templateVariables[$key];
		} else {
			$ret = null;
		}
		return $ret;
	}

	/**
	 * @desc A function to implement a virtual setter member for this class
	 *
	 * @param string $key
	 * @param mixed $value
	 * @return bool
	 *
	 * @see  http://www.ibm.com/developerworks/xml/library/os-php-flexobj/ by Jack Herrington <jherr@pobox.com>
	 */
	function __set ( $key, $value ) {
		$this->templateVariables[$key] = $value;
		return true;
	}

	public function __construct () {}

	public function __destruct () {}

	public function fetch ($includePath) {
		ob_start ();
		if (is_file ($includePath)) {
			$bIncluded = @include ($includePath);
		} else {
			ob_end_clean();
			throw new tsExceptionPackageImport ('Template ' . $includePath . ' could not be located');
			return '';
		}

		$this->content = ob_get_contents();
		ob_end_clean ();
		return $this->content;
	}

	public function assign ($varName, $value = null) {
		if (is_array($varName)) {
			$this->templateVariables += $varName;
		} else {
			$this->templateVariables[$varName] = $value;
		}
	}

}