<?php
/**
 * @package ts_views
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class views  extends tsPackage {
	protected $members = array (
		'tsTemplate',
	);
}