<?php
/**
 * Function to turn the triggered errors into exceptions
 * @author troelskn at gmail dot com
 * @see http://php.net/manual/en/class.errorexception.php
 * @param $severity
 * @param $message
 * @param $filename
 * @param $lineno
 * @throws ErrorException
 * @return void
 */
function exceptions_error_handler ($severity, $message, $filename, $lineno) {
  if (error_reporting() == 0) {
    return;
  }

  if (error_reporting() & $severity) {
    throw new ErrorException ($message, 0, $severity, $filename, $lineno);
  }
}

/**
 * @return bool
 */
function isCli () {
	return (php_sapi_name() == 'cli');
}

/**
 * returns an end of line, based on the environment
 * @return string
 */
function nl () {
	return isCli() ? "\n" : '<br/>'. "\n";
}

/**
 * Removes all extra spaces from a string
 * @param string $s
 * @return string
 */
function alltrim ($s) {
	return trim(preg_replace('/\s+/', ' ', $s));
}

function d () {
	$aRgs = func_get_args();
	$iExit = 1;

	if (!isCli()) {
		// not running in console
		echo '<pre>';
	}
	foreach ($aRgs as $object) {
		var_dump($object);
	}

	if (!isCli()) {
		// not running in console
		echo '</pre>';
	}
	die ();
}

/**
 * @see usingPackage
 * @param string $sPackageName
 * @return bool
 */
function import ($sPackageName) {
	return usingPackage($sPackageName);
}
/**
 * Adds the package name to the include path
 * @todo make sure that the path isn't aleady in the include path
 * @param string $packageName
 * @return bool
 * @throws tsExceptionPackageImport
 */
function usingPackage ($packageName) {
	$pkgLower 	= strtolower($packageName);
	$pkgPath	= LIB_PATH . DIRECTORY_SEPARATOR . $pkgLower;

	$path 		= get_include_path();
	if (is_dir($pkgPath)) {
		if (strpos ($path, $pkgPath) === false) {
			// adding exceptions dir to include path if it exists
			if (is_dir ($pkgPath. DIRECTORY_SEPARATOR . 'exceptions')) {
				// adding the exceptions if they exist
				$pkgPath .= PATH_SEPARATOR . $pkgPath . DIRECTORY_SEPARATOR . 'exceptions';
			}
			set_include_path(
				$path . PATH_SEPARATOR .
				$pkgPath
			);
		}
		return true;
	} else {
		usingPackage ('coreexceptions');
		throw new tsExceptionPackageImport ("bad package " . $packageName);
	}
}

function usingClass ($className) {
	if (class_exists ($className, false))  {
		return true;
	}

	$classNameLow = strtolower($className);

	$sFilePath	= $classNameLow . '.class.php';
	$fileIncluded = @include ($sFilePath);
	if ( !$fileIncluded ) {
		$sFilePath = $classNameLow . DIRECTORY_SEPARATOR . $sFilePath;
		$fileIncluded = @include ($sFilePath);
	}

	return $fileIncluded;
}

/**
* the __autoload automagic function for class initialization,
* see usingClass
*
* @param string $className
*/
function __autoload ($className) {
	if (class_exists ($className, false))  {
		return true;
	}
	return usingClass ($className);
}

/**
 * returns true if the user's ip is in our list of debug ips
 *
 * @return bool
 */
function isDebug (){
	if (stristr(C_SYSTEM_DEBUG_IPS, $_SERVER['REMOTE_ADDR']) && C_SYSTEM_DEBUG)
		return true;

	return false;
}

if (!function_exists('usingClass')) {
	/**
	 * The php4 function for including the class file
	 *
	 * @param string $className
	 * @return bool
	 */
	function usingClass($className) {
		if (class_exists($className))  {
			return true;
		}

		$classNameLow = strtolower($className);


		$classPaths = array (
			$classNameLow . '.class.php',	// regular homegrown class
			$classNameLow . DIRECTORY_SEPARATOR .$classNameLow . '.class.php',	// regular page
			$classNameLow . DIRECTORY_SEPARATOR . $classNameLow.'.class.php', // imported
		);

		foreach ($classPaths as $classPath) {
			$fileIncluded = @include($classPath);

			if ($fileIncluded) {
				return true;
			}
		}

		if (!$fileIncluded) {
			trigger_error ('Not found file containing '. $className .'.', E_USER_ERROR);
			return false;
		}

		if (!class_exists($className)) {
			trigger_error ('Unable to load class '.$className.' in path '.get_include_path().'.class.php', E_USER_ERROR);
			return false;
		}
	}
}

/**
 * Dumb function for email validation
 *
 * @param string $address
 * @return bool
 */
function emailIsValid ($address) {
	return (
		ereg(
			'^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+'. '@'. '[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+\.' . '[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$',
			$address
		)
	);
}

function getDirFiles ( $dir, $showHidden = false){

	$files =  array();
	if (!is_dir($dir)){
		trigger_error('Can not find : '.$dir);
		return false;
	}
	if ( $root = @opendir($dir) ){
		while ($file = readdir ($root)){
			if ( ($file == '.' || $file == '..') || ($showHidden == false && stripos($file, '.') === 0)){continue;}

			if (substr($dir, -1) != '/') $dir .= '/';

			if( is_dir ($dir . $file) ){
				$files = array_merge($files, getDirFiles($dir . $file));
			} else {
				/*if ( stristr($file, 'tpl') )*/ $files[] = $dir . $file;
			}
		}
	}
	return $files;
}

function isDBLink($incData) {
	if (sqlFactory::validType(DB_TYPE) && is_resource($incData) && stristr(get_resource_type($incData),DB_TYPE)) {
		return true;
	}
	return false;
}

if (!function_exists('mime_content_type')) {
	function mime_content_type ($filename) {
		$t = getimagesize($filename);

		if (is_array($t))
			return $t['mime'];
		else
			return 'application/unknown';
	}
}
