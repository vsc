<?php
/**
 * Class to allow usage of helper objects
 *
 * Theoretically it will have a multiple roles:
 * 1. combine a bit of business logic (eg, pagination computing) with display (eg, pagination HTML)
 * 2. mainly used for generating bits of HTML often used in the app
 *
 * It will need a clean merge with the Smarty or anything else I'll use for templating.
 * Not sure I need it as a Singleton. Maybe I can also use a factory.(?)
 *
 * @package ts_helpers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */

abstract class tsHelperAbstract {
	protected	$content,
				$template;

	private		$path;

	/**
	 * receives the template object
	 */
	public function __construct (&$template) {
		$this->template  = $template;
	}

	/**
	 * method to set the path of the template
	 */
	public function setPath ($incStr) {
		if (is_string ($incStr)) {
			$path = realpath ($incStr);
			if (is_file ($path)) {
				$this->path = $path;
				return true;
			}
		}
		return false;
	}

	public function getPath() {
		return $this->path;
	}

	public function build () {
		if (!empty ($this->path))
			return $this->template->fetch ($this->path);
		else
			return '';
	}

}
