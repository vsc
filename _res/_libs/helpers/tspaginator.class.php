﻿<?php
/**
 * @package ts_helpers
 * @desc Decorator class for tdoAbstract
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */

class tsPaginator extends tsHelperAbstract {
	const PAG_VAR	= 'pag';
	const PAG_COUNT	= 10;
	const PAG_ORDER	= 'o';

	private $start, $count, $total, $page, $order;
	/**
	 * @var tdoAbstract
	 */
	protected $tdo;

	public function __construct (&$template = null) {
		if ($template) {
			parent::__construct ($template);
			$this->setPath(VSC_PATH . DIRECTORY_SEPARATOR . '_res/_templates' . DIRECTORY_SEPARATOR . 'paginator.php');
		}
		$this->getPaginationVariables ();
	}

	public function __destruct () {}

	public function setCount ($int) {
		$this->count = $int;
		$this->start = $this->getStart();
	}

	public function outputOrderHeaders () {
		return;
	}

	/**
	 *	returns the pagination layout if a template is present
	 */

	public function build () {
		if (!$this->template)
			return '';
		$retVal	= $left = $right = '';
		$pages = array ();
		// this should happen after we made all our stuff with the tdo (joins, wheres, orders..bla)
		// how many stuff we got
		$t 		= $this->getTotal();
		$cnt	= ceil ($this->getTotal () / $this->count);
		$here	= tsController::getRequest(NAV_VAR);

		for ($i=1; $i <= $cnt; $i++) {
			$r = tsController::setRequest($here, array(self::PAG_VAR => $i));
			if ($i != $this->page) {
				$pages[$i]	= $r;
			} else {
				$pages[$i]	= '';
			}
		}

		// this will be replaced by the helper functionality of using templates :D
		$urlLeft	= tsController::setRequest($here, array (self::PAG_VAR => $this->page-1));
		$urlRight	= tsController::setRequest($here, array (self::PAG_VAR => $this->page+1));

		$this->template->assign ('pages', $pages);
		$this->template->assign ('cur_page', $this->page);
		$this->template->assign ('url_left', $urlLeft);
		$this->template->assign ('url_right', $urlRight);
		$this->template->assign ('max_pages', $cnt);

		try {
		$this->content  = $this->template->fetch ($this->getPath());
		} catch (tsExceptionPackageImport $e) {
			// todo
		}

		return $this->content;
	}

	public function addObject (&$object) {
		if (!($object instanceof tdoAbstract))
			return false;
		$this->tdo = &$object;

		if (!empty ($this->order))
			$this->addOrder ();
//		if (!empty ($this->start))
			$this->limit ();
	}

	protected function getTotal () {
		// getting the number of elements
		if (empty ($this->total) && ($this->tdo instanceof tdoAbstract)) {
			$this->total = (int)$this->tdo->getCount ();
		}
		return (int)$this->total;
	}

	protected function getStart () {
		if ((int)$this->page <= 0 /*/||
			(int)$this->start > $this->getTotal()/**/
		) {
			$this->page = 1;
		}
//		$this->start =
		// calculate the limit based on $total count of items and the pagination
		return $this->count * ($this->page-1) /*/+ 1/**/;//$this->start;
	}

	protected function getPaginationVariables () {
		// get the page variables: URL?p=?
		if (empty($this->count))
			$this->count = self::PAG_COUNT;

		$this->page		= (int)tsController::getRequest (self::PAG_VAR);

		$this->start	= $this->getStart ();

		// get the ORDER BY variables: URL?o=column_name:(bool)ASC
		$o = tsController::getRequest (self::PAG_ORDER);

		if (!empty ($o))
			$this->order	= explode (':', $o);

	}

	protected function addOrder () {
		// this is dumb as it requires a column name to comre from user input
		if ($this->tdo instanceof tdoAbstract && !empty ($this->order)) {
			$fieldName = $this->order[0];
			$order = (bool)$this->order[1];  // if we have no value we treat it as DESC

//			$this->tdo->addOrder ($fieldName, $order);
		}
	}

	protected function limit () {
//		var_dump($this->start, $this->count);
		if ($this->tdo instanceof tdoAbstract) {
			$this->tdo->limit ($this->start, $this->count);
		}
	}
}
