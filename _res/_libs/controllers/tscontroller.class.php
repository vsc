<?php
/**
 * @package ts_controllers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
usingPackage ('urlHelpers');
abstract class tsController {
	public	$theme		= null,
			$params		= array(),
			$cookieSet	= false,
			$time		= array('start' => 0,'end' => 0);

	static private $instance = null;

	static protected function memcacheEnabled () {
		// this sucks on so many ways
		if (extension_loaded ('memcache') && C_USE_MEMCACHE == true) {
			$memcache = new Memcache;
			if (@$memcache->connect('localhost', 11211)) {
				memcache_debug (true);
				return $memcache;
			}
		}
		return false;
	}

	/**
	 * function to redirect the client
	 *
	 * @param string $url
	 * @param bool $die
	 * @param bool $onlyJScript
	 */
	static public function redirect ($url, $die = true, $onlyJScript = false) {
		if ($onlyJScript == true || headers_sent()) {
			printf ('<?xml version="1.0" encoding="utf-8"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><head><title>Redirect</title><meta http-equiv="Refresh" content="0;url=%s" /></head><body onload="try {self.location.href=\'%s\' } catch(e) {}"><p><a href="%s">REDIRECTING - PLEASE CLICK HERE !</a></p></body></html>', $url, $url, $url);
		} else {
			ob_end_clean();
			header ('HTTP/1.0 302 Found');
			header ('Location: '.str_replace ('&amp;', '&', $url));
		}

		if ($die)
			die();
	}

	// FIXME: I should stop using REQUEST for getting get and post variables !!!
	static public function getRequest($varName = null) {
		return tsUrlFactory::getUrlHandler()->getRequest($varName);
	}

	/**
	 * method to establish if the current object has $incString as a valid action method
	 * FIXME : currently it searches trough all methods - including non actions
	 * @param string $incMember
	 * @return boolean
	 */
	public function isValidAction ($incMember = null) {
		if (
			$incMember &&
			in_array ($incMember, get_class_methods(get_class($this)))
			// be careful, if you declare actions as private this doesn't work - doh!
		) {
			return true;
		} else
			return false;
	}
	/**
	 * function to generate URLs compatible with tsController
	 *
	 * @param string $whereTo the page name
	 * @param string $varVal additional params
	 * @param string $method [get|TODO post]
	 * @return string
	 */
	static public function setRequest ($whereTo = array(), $varVal = array()){
		if (!is_array($whereTo))
			$whereTo = array ($whereTo);

		return tsUrlFactory::getUrlHandler()->setRequest (array_merge($whereTo, $varVal));
	}

	/**
	 *	function to set the persistent URL parameters
	 *	@param array $incArray
	 */
	public function setParams ($incArray = array ()) {
		if (!empty ($incArray))
			$this->params = array_merge ($this->params, $incArray);

		return false;
	}

	public function getParams () {
		return $this->params;
	}

	public function store () {

	}

	/**
	 * static method to return the tsController singleton instance
	 *
	 * @param string $to
	 * @return tsController
	 */
	static function getInstance ($to, $args = null) {
		// we have already initialized the page singleton
		if (self::$instance instanceof tsController) {
			return self::$instance;
		} else {
			// the memcache object should also be a singleton
			// with a static instance I can call wherever.
//			if (extension_loaded ('Memcache')) {
//				$memcache = tsController::memcacheEnabled ();
//				if ( $memcache) {
//					tsController::$instance = $memcache->get ( tsController::getHash ());
//				}
//			}
			if (!(self::$instance instanceof tsController) && class_exists($to)) {
//				var_dump ($to, $args);die;
				self::$instance = new $to ($args);
				if (!empty($regex))
					self::$instance->addRegex ($regex);
			} elseif (!class_exists ($to)) {
				throw new tsExceptionPackageImport ($to .  'is not in the include path', E_USER_ERROR);
			}
		}
		return self::$instance;
	}

	static public function getHash () {
		if (empty ($_SERVER['QUERY_STRING'])) {
			if (stristr($_SERVER['SERVER_SOFTWARE'], 'Apache'))
				$redirectUri = 'REDIRECT_URL';
			elseif (stristr($_SERVER['SERVER_SOFTWARE'], 'lighttpd'))
				$redirectUri = 'REDIRECT_URI';
			else
			 	$redirectUri = 'REDIRECT_URI';
			$qstring = $_SERVER[$redirectUri];
		} else {
			$qstring = $_SERVER['QUERY_STRING'];
		}
		return md5($_SERVER['HTTP_HOST'] . $qstring);
	}

	protected function setHeaders () {
		$cacheTime = 2419200; // 30 days
		// on dreamhost it seems we have some default cache control stuff we need to overwrite
		header ('Cache-Control: max-age=' . $cacheTime . ', public, must-revalidate');
		header ('Pragma: public');

		// set expire header.
		// header ('Expires: ' . strftime ('%a, %d %b %Y %T GMT', time() + $cacheTime));
	}

	private function addToCache () {
		if (!extension_loaded ('Memcache'))
			return false;
		$memcache = tsController::memcacheEnabled ();
		$hash = tsController::getHash ();
		if ($memcache instanceof Memcache && !$memcache->get ($hash)) {
			$memcache->connect('localhost', 11211);
			$memcache->add ($hash , $this, MEMCACHE_COMPRESSED);
		}
	}

	public function getTheme(){
		// this should most probably be changed to tsController::getRequest ('theme')
		if (!empty($_GET['theme']) && isDebug()) {
			$this->theme		= $_GET['theme'];
		} elseif (!empty($_POST['theme'])) {
			$this->theme		= $_POST['theme'];
		} elseif (!empty($_COOKIE['theme'])) {
			$this->theme		= $_COOKIE['theme'];
		}
		if (empty($this->theme) ||
			!is_dir(THEME_PATH.$this->theme) ||
			!is_dir(THEME_PATH.$this->theme . DIRECTORY_SEPARATOR . "_css") ||
			!is_dir(THEME_PATH.$this->theme . DIRECTORY_SEPARATOR . "_templates")
		) {
			$this->theme		= DEFAULT_THEME;
		}

		return $this->theme;
	}

	public function __construct () {}

	public function __destruct () {}

	public function dispatch () {
		$this->setHeaders();
//		$this->addToCache();
	}
}
