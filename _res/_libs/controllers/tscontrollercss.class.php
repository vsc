<?php
/**
 * @package ts_controllers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */

class tsControllerCss extends tsController {
	public		$themePath;

	private		$media = 'screen';

	/**
	 * the constructor that does it all.
	 * starts output buffering, and initializes different things
	 *
	 */
	public function __construct(){
		ob_start();

		$this->theme 		= (!empty($_COOKIE['theme']) ? $_COOKIE['theme'] : DEFAULT_THEME);

		$this->media 		= tsController::getRequest('do');

		if (empty($this->media))
			$this->media =  'screen';

		if (!is_file(THEME_PATH. $this->theme . DIRECTORY_SEPARATOR .'_css'. DIRECTORY_SEPARATOR .$this->media.'.css')) {
			$this->theme		= DEFAULT_THEME;
			$this->media		= 'screen';
		}

	}

	public function dispatch (){
		parent::dispatch ();
		header('Content-type: text/css');
		$out  = file_get_contents ( THEME_PATH. $this->theme . DIRECTORY_SEPARATOR .'_css'. DIRECTORY_SEPARATOR .$this->media.'.css');
		$this->postDispatch ( $out );
	}


	private function postDispatch ($incString){
		// set expire header.
		header ('Expires: '.strftime ('%a, %d %m %Y %T GMT', time()+2419200)); // one month
		if (stristr ($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
			$incString = str_replace(array('min-width:', 'max-width:'), 'width:', $incString);
		}
//		echo $incString;
		echo str_replace (
			array ('%TIME%','%QUERIES%', '%MEMUSED%'),
			array (
				number_format(microtime (true)  - $GLOBALS['st'] , 5, ',', '.'),
				$GLOBALS['qCnt'],
				number_format(memory_get_usage()/1024, 3, ',', '.')
			),
			$incString
		);
	}
}
