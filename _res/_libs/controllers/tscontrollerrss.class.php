<?php
/**
 * @package ts_controllers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */

class tsControllerRss extends tsControllerHtml {
	public $db;
	public function __construct(){
		ob_start ();
		$this->setTheme();

		session_start();

		$this->connectDb();
	}

	protected function connectDb () {
		$this->db = sqlFactory::connect (DB_TYPE);

		if (defined ('DB_NAME') && DB_NAME && !empty($this->db->link)) {
			$this->db->selectDatabase(DB_NAME);
			return true;
		}
		return false;
	}

	public function dispatch (){
		$this->db->close();
		$this->setTheme();

		$className = 'rss';

		$this->view = new tsTemplate();
		$this->view->assign('url', 			'http://'.$_SERVER['HTTP_HOST']);

//		if (is_file (PAGE_PATH . $className . DIRECTORY_SEPARATOR . $className . '.php')) {
//			$this->view->assign ('contentFile', PAGE_PATH . $className . DIRECTORY_SEPARATOR . $className . '.php');
//		} else {
//			$this->view->assign ('contentFile', $this->themePath . DIRECTORY_SEPARATOR . S_TEMPL_DIR . 'tpl404.php');
//		}

		if (is_array($this->varArray)) {
			$this->view->assign ($this->varArray);
		}
		$this->content = $this->view->fetch ($this->themePath . DIRECTORY_SEPARATOR . S_TEMPL_DIR . 'rss.php');
		$this->postDispatch ($this->content);
	}

	protected function postDispatch ($incString) {
		ob_end_clean();
		ob_end_clean();
		header ('Content-Type: application/rss+xml');
		// set expire header.
		header ('Expires: '.strftime ('%a, %d %m %Y %T GMT', time()+2419200)); // one month

		echo str_replace (
			array ('%TIME%','%QUERIES%', '%MEMUSED%'),
			array (
				number_format(microtime (true)  - $GLOBALS['st'] , 5, ',', '.'),
				$GLOBALS['qCnt'],
				number_format(memory_get_usage()/1024, 3, ',', '.')
			),
			$incString
		);
	}
}
