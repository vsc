<?php
/**
 * @package ts_controllers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */

usingPackage ('views');
usingPackage ('models');
usingPackage ('models/sqlDrivers');

/**
 * @name	tsControllerHtml
 * @package html Output class
 * @desc	the God class. It does everything, from database initialization
 * 				to error reporting and page buffering. <- yeah it might be stupid
 *
 * @public	the smarty template class instance and the theme path
 * @private	the page identification
 */

class tsControllerHtml extends tsController {
	protected	$themePath,
				$db,
				$content,
				$varArray	= array(),
				$view;

	private		$to,
				$output,
				$errors;

	/**
	 * the constructor that does it all.
	 * starts output buffering, and initializes different things
	 *
	 */
	public function __construct(){

		$prevOutput			= ob_get_clean(); // errors previous to the tsController init
		// TODO: setting the display mode (XHTML/WML/etc) based on some rules.
//		$this->output 		= $this->setOutput();
		// Override standard string functions with UTF8 ones - not working
//		if (extension_loaded('mbstring')) {
//			ini_set('mbstring.func_overload', 7);
//		}

		ob_start ();
		$this->view 					= new tsTemplate ();

//		set_error_handler(array($this, 'triggerError'));
// commented in the light of the xdebug profiling -
// dropped loading times by .01 seconds
		if (!empty($prevOutput)) {
			$this->errors		= "\n".'<!-- output previous init at '.date('G:i:s').' -->'."\n".$prevOutput."\n";
		} else {
			$this->errors		= '';
		}
		$this->setTheme();

		session_start();
		$this->connectDb();
	}

	function __destruct(){
//		$this->db->close();
	}

	protected function connectDb ($dbName = null) {
		$this->db = sqlFactory::connect (DB_TYPE);
		if (defined ('DB_NAME') && DB_NAME && !empty($this->db->link)) {
			if (empty($dbName))
				$dbName = DB_NAME;
			$this->db->selectDatabase($dbName);
			return true;
		}

		return false;
	}

	public function setTheme (){
		$this->getTheme();

		$this->themePath		= THEME_PATH . $this->theme;
		if (!is_dir($this->themePath)) {
			$this->theme 		= DEFAULT_THEME;
			$this->themePath	= THEME_PATH . $this->theme;
		}

		$this->setThemeCookie();
	}

	private function setThemeCookie () {
		if (empty ($_COOKIE['theme']) || $_COOKIE['theme'] != $this->theme) {
			$test = setcookie ('theme', $this->theme, time()+1296000, '/');
		}
	}

	private function reportErrors (){
		if (!stristr (C_SYSTEM_DEBUG_IPS, $_SERVER['REMOTE_ADDR'])) {
			return false;
		}
		if (!empty ($this->errors)) {
			$errors = $this->errors;
			$this->errors = '<!-- ERRORS: '.get_class($this).' at '.date('G:i:s').'-->'."\n".$errors;
		}

		if (defined ('ERROR_LEVEL') && ERROR_LEVEL == 9 )
			$this->errors .= "\n".var_export (debug_backtrace(), true);
		switch (C_SYSTEM_DEBUG_METHOD){
			case 0:		// output
				$this->view->assign('errors', $this->errors);
				return true;
				break;
			case 1:		// email
				$mailMsg		= $this->errors;
//				mail (DEBUG_MAIL,'DEBUG:',$mailMsg);
				return true;
//				break;
		}
		return false;
	}

	public function triggerError($errNo=0, $errStr='', $errFile='', $errLine=''){
		if (empty($this->errors)){
			$this->errors = "\n";
		}
		switch ($errNo){
			case (2):
				$errType = 'WARNING';
				break;
			case (8):
				$errType = 'NOTICE';
				break;
			case (256): // errNo = 4
				$errType = 'USER_ERROR';
				break;
			case (512): // errNo = 8
				$errType = 'USER_WARNING';
				break;
			case (1024): // errNo = 16
				$errType = 'USER_NOTICE';
				break;
			case (2048): // errNo = 16
				$errType = 'USER_ERROR';
				break;
			default:
				var_dump($errNo);
		}
		if ($errNo < 255) {
			$baseColor = array('R'=>100,'G'=>16,'B'=>16);
			$color = (string)dechex($baseColor['R']+40 * (log($errNo, 2))).(string)dechex($baseColor['G']+(20 - 4*log($errNo, 2))*(log($errNo, 2))).(string)dechex($baseColor['B']+(20 - 4*log($errNo, 2))*(log($errNo, 2)));
		} else {
			$baseColor = array('R'=>16,'G'=>16,'B'=>100);
			$errNo = $errNo / 128;
			$color = (string)dechex($baseColor['R']+(20 - 4*log($errNo, 2)) * (log($errNo, 2))).(string)dechex($baseColor['G']+(20 - 4*log($errNo, 2))*(log($errNo, 2))).(string)dechex($baseColor['B']+40*(log($errNo, 2)));
		}

		$this->errors .= '<span style="color:#'.strtoupper($color).'">';
		$this->errors .= '<b>'.$errType.'</b> '.$errStr;
		if (false || tsController::getRequest('bt') == 'full') {
			$this->errors .= ' at line '.$errLine.' in file '.$errFile;
			$t = debug_backtrace();
//			$this->errors .= '<pre>'.var_export($t,true).'</pre>';
		}

		$this->errors .= ' </span><br/>'."\n";

		return;
	}
	public function dispatch () {
		// this should have been here a long time ago
		if ($this->db instanceof sqlFactory)
			$this->db->close();

		$this->setTheme();

		parent::dispatch ();

		if ($this->view != null) {
		
		$this->view->assign ('header', $this->themePath . DIRECTORY_SEPARATOR. S_TEMPL_DIR . 'header.php');
		$this->view->assign ('footer', $this->themePath . DIRECTORY_SEPARATOR. S_TEMPL_DIR . 'footer.php');

		$this->view->assign ('url', 			tsController::setRequest());
		$this->view->assign ('style', 			'');
		$this->view->assign ('keywords', 		'');
		$this->view->assign ('description', 	'');
		$this->view->assign ('contact', 		tsController::setRequest('contact'));
		
		$className	= get_class ($this);
//		$methArr 	= get_class_methods ($this);

		/* Loading of any varArray template variables we have in the derived classes
		--------------------------------------------------------------------- */
		if (is_array($this->varArray)) {
			$this->view->assign ($this->varArray);
		}

		// in the case where we didn't assign already a contentFile to be used we switch to the default one
		if (!$this->view->contentFile) {
			$this->view->contentFile = PAGE_PATH . DIRECTORY_SEPARATOR . $className . DIRECTORY_SEPARATOR . $className . '.php';
		}

		// if there is no default one and the content variable is empty we trigger a 404
		// TODO: move this to the router
		if (!is_file ($this->view->contentFile) && empty ($this->view->content)) {
			$this->view->assign ('contentFile', $this->themePath . DIRECTORY_SEPARATOR . S_TEMPL_DIR . '404.php');
		}


		if (!C_SYSTEM_DEBUG) {
			ob_end_clean();
			ob_start();
		} else {
			$this->errors .= ob_get_clean();
			ob_start();		// clean output - heheh, yeah right :P
			$this->reportErrors ();
		}

		$this->content 		= $this->view->fetch ($this->themePath . DIRECTORY_SEPARATOR . S_TEMPL_DIR . 'main.php');
		}
		$this->postDispatch ($this->content);
	}


	private function postDispatch ($incString){
		// check to see if we're a 404 error
		ob_end_clean ();
		ob_start();
		if (get_class ($this) == 'tsControllerHtml') {// yes 404
			header ("HTTP/1.0 404 Not Found");
		}

		if ( stristr($_SERVER['HTTP_ACCEPT'],'application/xhtml+xml') && !C_SYSTEM_DEBUG ) {
  			header('Content-type: application/xhtml+xml');
		} else {
  			header('Content-type: text/html');
		}

		echo str_replace (
			array ('%TIME%','%QUERIES%', '%MEMUSED%'),
			array (
				number_format((microtime (true)  - $GLOBALS['st']) * 1000, 9, ',', '.'),
				$GLOBALS['qCnt'],
				number_format(memory_get_usage()/1024, 3, ',', '.')
			),
			$incString
		);
	}
}
