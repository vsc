<?php
/**
 * @package ts_controllers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class ctrlFactory {
	static public $validTypes = array ('html', 'image', 'css', 'rss');

	static public function isValidType ($type) {
		return in_array ($type, ctrlFactory::$validTypes);
	}

	static public function getController ($type = 'html') {
		if (!ctrlFactory::isValidType($type)) return false;

		$to = tsUrl::getRequest (NAV_VAR);

		if ($to == 'image') {
			$type = 'image';
		}
		if ($to == 'style') {
			$type = 'css';
		}
		if ($to == 'rss') {
			$type = 'rss';
		}


		if ($type == 'html') {
			// regular HTML content - later can be extended into returning
			// different controlers based on user agent (?)
			$to = tsUrl::getRequest (NAV_VAR);

			return new $to ();
		} elseif ($type == 'css') {
			// return style sheet controller
			return new tsControllerCss ();
		} elseif ($type == 'rss') {
			// return RSS controller
			return new tsControllerRss ();
		} elseif ($type == 'image') {
			$do = tsController::getRequest(ACT_VAR);
			if (!empty($do) && is_dir(PAGE_PATH . $do) && is_file(PAGE_PATH . $do . DIRECTORY_SEPARATOR . 'image.php')) {
				include (PAGE_PATH . $do . DIRECTORY_SEPARATOR . 'image.php');
			}else {
				$do = 'tsControllerImage';
			}
			return new $do ();
		} else {
			// no valid controller found
			return false;

		}
	}
}
