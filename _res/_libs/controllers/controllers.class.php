<?php
/**
 * @package ts_controllers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */
class controllers extends tsPackage {
	protected $members = array (
		'ctrlFactory',
		'tsController',
		'tsControllerCss',
		'tsControllerRss',
		'tsControllerHtml',
		'tsControllerImage',
	);

	protected $path;

	public function __construct () {
		$this->path = dirname(__FILE__);
	}
}