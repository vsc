<?php
/**
 * @package ts_controllers
 * @author Marius Orcsik <marius@habarnam.ro>
 * @date 09.02.27
 */

class tsControllerImage extends tsController {
	public static	$allowedExtensions 	= array (1 => 'gif', 2 => 'jpg', 3 => 'png');
	public			$path;
	protected 		$basePath;
	private 		$content,
					$imgAttr,
					$hashedPath 		= null;

	public function __construct () {
		ob_start();
		$this->time['start'] 	= microtime (true);
		$this->path = $this->getPath ();
	}

	protected function getPath () {
		if (empty($this->basePath)) {
			$this->basePath = PAGE_PATH;
		}/* else {
			$this->basePath .= DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
		}*/

//		list ($to, $do, $file) = tsController::getRequest();
//		var_dump( $this->basePath);die;

		foreach ( tsControllerImage::$allowedExtensions as $ext) {
			$path = $this->basePath.'.'.$ext;
//			var_dump($path);die;
			if (is_file ($path)) {
				return $path;
			}
		}
//		die;
		return false;
	}

	public function __destruct (){
		if ( !empty($this->content) )
			imagedestroy($this->content);
	}

	public function testPath ($incPath = null) {
		if ($incPath == null)
			$incPath = $this->path;

		$ext	= end(explode('.', $incPath));

		if (in_array ($ext, tsControllerImage::$allowedExtensions)){
			$realPath	= realpath ($incPath);
			if (file_exists($realPath)){
				$this->path = $realPath;
				return true;
			}
		}
		return false;
	}

	public function getType(){
		$this->imgAttr = getimagesize ($this->path);
		switch ($this->imgAttr[2]) {
			case 1 :
				$this->content = imagecreatefromgif ($this->path);
				break;
			case 2 :
				$this->content = imagecreatefromjpeg ($this->path);
				break;
			case 3 :
				$this->content = imagecreatefrompng ($this->path);
				imageAlphaBlending($this->content, true);
				imageSaveAlpha($this->content, true);
				break;
			default :
				break;
		}
	}

	public function outputHash(){
		$retVal = false;

		if (tsControllerImage::$allowedExtensions[$this->imgAttr[2]] == 'jpg')
			$type = 'jpeg';
		else
			$type = tsControllerImage::$allowedExtensions[$this->imgAttr[2]];

		$functionName = 'image'.$type;

		if (!is_file($this->hashedPath) && !empty($this->content)) {
			header ('Content-type: image/'.$type);
			$retVal = $functionName($this->content, $this->hashedPath);
		}


		return $retVal;
	}
	public function resize($w, $h = null, $method = 'scale'){
		if (empty($this->content))
			return false;

		// check cache
		if (!empty($w))
			$this->hashedPath = S_C_TEMPL_DIR.md5($this->path.$w.$h);

//		die(print_r($this->hashedPath));
		if (is_file($this->hashedPath)) {
			$imgAttr = getimagesize($this->hashedPath);
//			if ($w == $imgAttr[0] && $h == $imgAttr[1] && !empty($allowedExtensions[$imgAttr[2]])) {
				$functionName	= 'imagecreatefrom'.(tsControllerImage::$allowedExtensions[$imgAttr[2]] == 'jpg' ? 'jpeg' : tsControllerImage::$allowedExtensions[$imgAttr[2]]);
				$this->content	= $functionName($this->hashedPath);
//			} else {
//				$this->outputHash();
//			}
		} else {
		 	$temp = $this->content;

			if (empty($h))		// only width... we will make it proportional
				$h	= round($this->imgAttr[1]/($this->imgAttr[0]/$w));

			$this->content = imagecreatetruecolor ($w, $h);
			$retVal	= imagecopyresampled ($this->content, $temp, 0, 0, 0, 0, $w, $h, $this->imgAttr[0], $this->imgAttr[1]);

			$this->outputHash();
		}
		if (!empty($retVal)) {
			return true;
		} else {
			$this->content = $temp;
			return false;
		}
	}

	public function dispatch (){
		parent::dispatch ();
		ob_end_clean();

		$this->getType();

		if (!$this->testPath())
			return false;

		if (!is_array($this->imgAttr))
			trigger_error ($this->path.' is not an image', E_USER_ERROR);
		if (tsControllerImage::$allowedExtensions[$this->imgAttr[2]] == 'jpg')
			$type = 'jpeg';
		else
			$type = tsControllerImage::$allowedExtensions[$this->imgAttr[2]];

		$functionName = 'image'.$type;
		header ('Content-type: '.$this->imgAttr['mime']);

		if (!empty($w))
			$this->hashedPath = S_C_TEMPL_DIR . md5($this->path.$w.$h);

		if (!is_file($this->hashedPath)) {
			ob_start();
			$retVal = $functionName($this->content);
			$imageData = ob_get_contents();
			echo $imageDataLength = ob_get_length();
			ob_end_clean();

			header ('Content-Length: ' . $imageDataLength);
			echo $imageData.'Not Hashed';
		} else {
			header ('Content-Length: ' . filesize($this->hashedPath));
			echo $retVal = readfile($this->hashedPath);
			echo 'Hashed';
		}
		$this->time['end'] 	= microtime (true);
//		return $retVal;
	}

	public function debug(){
		ob_end_clean();
		$this->time['end'] 	= microtime (true);
		$this->time['ms']	= number_format(($this->time['end'] - $this->time['start']) * 1000 , 2, ',' , '.');
		echo ('Content-type: '.$this->imgAttr['mime'])."\n";
		echo '<pre>'."\n";
		var_dump($this);
		echo '</pre>'."\n";
	}
}
