<?php// if ($this->pages) {?>
<span class="paginator_left">
	<?php if ($this->cur_page > 1) {?><a href="<?php echo $this->url_left?>">&laquo;</a> <?php } else { echo '&laquo;'; }?>
</span>
<?php foreach ($this->pages as $page => $url) { ?>
	<?php if ($url) {?> <a href="<?php echo $url ?>"><?php echo $page ?></a> <?php } else { echo $page; } ?>
<?php }?>
<span class="paginator_right">
	<?php if ($this->cur_page < $this->max_pages) { ?><a href="<?php echo $this->url_right ?>">&raquo;</a> <?php } else {echo '&raquo;';} ?>
</span>
<?php// }?>
