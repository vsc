<?php
usingPackage ('coreexceptions');

class fooFunctionsTest extends UnitTestCase {

	private $state;

	public function setUp () {
		$this->state = get_include_path();
		set_include_path ('.');
	}

	public function tearDown() {
		set_include_path($this->state);
	}

	public function test_usingPackageReturnPath () {
		usingPackage ('models');

		$this->assertEqual(get_include_path(), '.' . PATH_SEPARATOR . LIB_PATH.'/models');
	}

	public function test_usingPackageBadPackage () {
		$e = 0;
		$sPackageName = '...';
		try {
			usingPackage ($sPackageName);
		} catch (Exception $e) {
			$this->assertIsA($e, 'tsExceptionPackageImport', 'The usingpackage function didn\' throw the correct exception.');
		}
	}
}