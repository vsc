#!/usr/bin/php
<?php
include ('_tests/config.inc.php');
include ('vsc.inc.php');
ini_set ('display_errors', '1'); error_reporting (E_STRICT & E_ALL);
set_error_handler('exceptions_error_handler');

try {
	import ('imported/simpletest');
	include_once ('unit_tester.php');
	include_once ('mock_objects.php');
	include_once ('collector.php');
	include_once ('default_reporter.php');
} catch (tsExceptionPackageImport $e) {
	// problems with the simpletest imported package
	echo $e->getMessage() . "\n";
}

try {
	usingPackage ('tests');
	$result = tsUnitTest::execute ($_SERVER['argc'], $_SERVER['argv']);
} catch (tsExceptionPackageImport $e) {
	// could not import the tests package
	echo $e->getMessage() . "\n";
	echo $e->getFile() . ' - ' .$e->getLine () ."\n";
	echo $e->getTraceAsString();
	exit (1);
} catch (tsExceptionAutoload $e) {
	// could not load the tsUnitTest class
	echo $e->getMessage() . "\n";
	exit (1);
}

if (SimpleReporter::inCli()) {
	exit ($result ? 0 : 1);
}